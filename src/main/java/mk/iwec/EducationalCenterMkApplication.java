package mk.iwec;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EducationalCenterMkApplication {

	public static void main(String[] args) {
		SpringApplication.run(EducationalCenterMkApplication.class, args);
	}

}
