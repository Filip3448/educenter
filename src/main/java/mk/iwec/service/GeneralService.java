package mk.iwec.service;

import java.util.List;

public interface GeneralService<T,ID> {
	public T findById(ID id);
	public List<T> findAll();
	public T create (T entity);
	public T update(ID id, T entity);
	public void deleteById(ID id);
}
