package mk.iwec.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;

import lombok.extern.slf4j.Slf4j;
import mk.iwec.model.Lecturer;
import mk.iwec.repository.LecturerRepository;

@Service
@Slf4j
@Transactional
public class LecturerServiceImpl implements GeneralService<Lecturer, Integer> {

	@Autowired
	public LecturerRepository lecturerRepo;
	@Override
	public Lecturer findById(Integer id) {
		Lecturer entity = lecturerRepo.findById(id).orElseThrow();
		return entity;
	}

	@Override
	public List<Lecturer> findAll() {
		log.debug("Find All Lecturers");
		return lecturerRepo.findAll();
	}

	@Override
	public Lecturer create(Lecturer entity) {
		log.debug("Create lecturer with paramaters", entity);
		Lecturer persistedEntity = lecturerRepo.saveAndFlush(entity);
		return persistedEntity;
	}

	@Override
	public Lecturer update(Integer id, Lecturer entity) {
		if(lecturerRepo.findById(id).isPresent()) {
			entity.setId(id);
			return lecturerRepo.save(entity);
			}
			throw new ResourceAccessException("Lecturer is not found");
	}

	@Override
	public void deleteById(Integer id) {
		log.debug("Delete Lecturer with Id", id);
		lecturerRepo.deleteById(id);
		
	}

}
