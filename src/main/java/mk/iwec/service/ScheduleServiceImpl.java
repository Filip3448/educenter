package mk.iwec.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;

import lombok.extern.slf4j.Slf4j;
import mk.iwec.model.Schedule;
import mk.iwec.repository.ScheduleRepository;

@Service
@Slf4j
@Transactional
public class ScheduleServiceImpl implements GeneralService<Schedule,Integer> {
	
	@Autowired
	public ScheduleRepository schedulerepo;
	
	@Override
	public Schedule findById(Integer id) {
		Schedule entity = schedulerepo.findById(id).orElseThrow();
		return entity;
	}

	@Override
	public List<Schedule> findAll() {
		log.debug("Find All Schedules");
		return schedulerepo.findAll();
	}

	@Override
	public Schedule create(Schedule entity) {
		log.debug("Create new Schedule with parametars", entity);
		Schedule persistedEntity = schedulerepo.save(entity);
		return persistedEntity;
	}

	@Override
	public Schedule update(Integer id, Schedule entity) {
		if(schedulerepo.findById(id).isPresent()) {
			entity.setId(id);
			return schedulerepo.save(entity);
			}
			throw new ResourceAccessException("Schedule is not found");
	}

	@Override
	public void deleteById(Integer id) {
		log.debug("Schedule with Id", id);
		schedulerepo.deleteById(id);
		
	}

}