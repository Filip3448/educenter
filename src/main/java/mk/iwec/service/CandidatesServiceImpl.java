package mk.iwec.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;

import lombok.extern.slf4j.Slf4j;
import mk.iwec.model.Candidates;
import mk.iwec.repository.CandidateRepository;

@Service
@Slf4j
@Transactional
public class CandidatesServiceImpl implements GeneralService<Candidates, Integer> {
	
	@Autowired
	public CandidateRepository repository;
	
	@Override
	public Candidates findById(Integer id) {
		Candidates entity = repository.findById(id).orElseThrow();
		return entity;
	}

	@Override
	public List<Candidates> findAll() {
		log.debug("Find All Candidates");
		return repository.findAll();
	}

	@Override
	public Candidates create(Candidates entity) {
		log.debug("Create Candidate with parametars", entity);
		Candidates persistedEntity = repository.save(entity);
		return persistedEntity;
	}

	@Override
	public Candidates update(Integer id, Candidates entity) {
		if(repository.findById(id).isPresent()) {
		entity.setId(id);
		return repository.save(entity);
		}
		throw new ResourceAccessException("Candidate is not found");
	}

	@Override
	public void deleteById(Integer id) {
		log.debug("Delete Candidate with Id", id);
		repository.deleteById(id);
		
	}
}