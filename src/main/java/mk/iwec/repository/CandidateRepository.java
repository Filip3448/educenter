package mk.iwec.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mk.iwec.model.Candidates;
@Repository
public interface CandidateRepository extends JpaRepository<Candidates, Integer>{

}
