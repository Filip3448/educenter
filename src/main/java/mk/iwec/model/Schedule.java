package mk.iwec.model;

import java.util.ArrayList;
import java.util.List;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity

@Table(name = "schedule",
uniqueConstraints=
@UniqueConstraint(columnNames={"location", "schedule"}))

public class Schedule {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "course", nullable = false)
	private String course;

	@Column(name = "location", nullable = false)
	private String location;

	@Column(name = "schedule", nullable = false)
	private String schedule;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "schedule", fetch = FetchType.LAZY )
	private List<Lecturer> lecturers = new ArrayList<>();
}
