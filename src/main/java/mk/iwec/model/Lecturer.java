package mk.iwec.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "lecturer")
public class Lecturer {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "first_name", nullable = false)
	private String lecturerfirstName;

	@Column(name = "last_name", nullable = false)
	private String lecturerlastName;

	@Column(name = "email", nullable = false, unique = true)
	private String lectureremail;
	
	@ManyToOne
	@JoinColumn(name = "schedule_id")
	@JsonIgnore
	private Schedule schedule;
	
}
