package mk.iwec.exceptions;

import java.sql.Timestamp;

import org.springframework.http.HttpStatus;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExceptionResponse {
	private HttpStatus httpStatus;
	private String errorMessage;
	private Timestamp timestamp;
}

