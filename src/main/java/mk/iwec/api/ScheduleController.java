package mk.iwec.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import mk.iwec.model.Schedule;
import mk.iwec.service.ScheduleServiceImpl;
@CrossOrigin
@RestController
@RequestMapping(path = "/schedule")
public class ScheduleController {

	@Autowired
	private ScheduleServiceImpl scheduleserv;
	
	@GetMapping("/{id}")
	public Schedule findById(@PathVariable(value = "id") Integer id) {
		return scheduleserv.findById(id);
	}
	@GetMapping("/list")
	public List<Schedule> findAll() {
		return scheduleserv.findAll();
	}
	@PostMapping("/new")
	@ResponseStatus(value = HttpStatus.CREATED)
	public Schedule create(@RequestBody Schedule entity) {
		return scheduleserv.create(entity);
	}

	@PutMapping("/{id}")
	@ResponseStatus(value = HttpStatus.OK)
	public Schedule update(@PathVariable(value = "id") Integer id, @RequestBody Schedule entity) {
		return scheduleserv.update(id, entity);
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void deleteById(@PathVariable(value = "id") Integer id) {
		scheduleserv.deleteById(id);
	}
}