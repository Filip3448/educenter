package mk.iwec.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import mk.iwec.model.Candidates;
import mk.iwec.service.CandidatesServiceImpl;

@CrossOrigin
@RestController
@RequestMapping(path = "/candidates")
public class CandidatesController {

	@Autowired
	private CandidatesServiceImpl candidateserv;

	@GetMapping("/{id}")
	public Candidates findById(@PathVariable(value = "id") Integer id) {
		return candidateserv.findById(id);
	}

	@GetMapping("/list")
	public List<Candidates> findAll() {
		return candidateserv.findAll();
	}

	@PostMapping("/new")
	@ResponseStatus(value = HttpStatus.CREATED)
	public Candidates create(@RequestBody Candidates entity) {
		return candidateserv.create(entity);
	}

	@PutMapping("/{id}")
	@ResponseStatus(value = HttpStatus.OK)
	public Candidates update(@PathVariable(value = "id") Integer id, @RequestBody Candidates entity) {
		return candidateserv.update(id, entity);
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void deleteById(@PathVariable(value = "id", required = true) Integer id) {
		candidateserv.deleteById(id);
	}
}
