package mk.iwec.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import mk.iwec.model.Lecturer;
import mk.iwec.service.LecturerServiceImpl;
@CrossOrigin
@RestController
@RequestMapping(path = "/lecturer")
public class LecturerController {

	@Autowired
	private LecturerServiceImpl lecturerserv;

	@GetMapping("/{id}")
	public Lecturer findById(@PathVariable(value = "id") Integer id) {
		return lecturerserv.findById(id);
	}

	@GetMapping("/list")
	public List<Lecturer> findAll() {
		return lecturerserv.findAll();
	}

	@PostMapping("/new")
	@ResponseStatus(value = HttpStatus.CREATED)
	public Lecturer create(@RequestBody Lecturer entity) {
		return lecturerserv.create(entity);
	}

	@PutMapping("/{id}")
	@ResponseStatus(value = HttpStatus.OK)
	public Lecturer update(@PathVariable(value = "id") Integer id, @RequestBody Lecturer entity) {
		return lecturerserv.update(id, entity);
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void deleteById(@PathVariable(value = "id") Integer id) {
		lecturerserv.deleteById(id);
	}
}