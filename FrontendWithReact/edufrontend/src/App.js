import React from 'react';
import logo from './logo.svg';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';

import NavigationBar from './components/navigationbar.component';
import CandidateList from './components/allcandidates.component';
import LecturerList from './components/alllecturer';
import ScheduleList from './components/allschedule';
import UpdateCandidate from './components/updatecandidate';
import UpdateLecturer from './components/updatelecturer';
import UpdateSchedule from './components/updateschedule';
import CreateCandidate from './components/createcandidate';
import CreateLecturer from './components/createlecturer';
import CreateSchedule from './components/createschedule';
import DeleteCandidate from './components/deletecandidates';
import DeleteLecturer from './components/deletelecturer';
import DeleteSchedule from './components/deleteschedule';



function App() {
  return (
    <Router>
	<NavigationBar/>
	<Route path="/candidates" component={CandidateList} />
	<Route path="/lecturer" component={LecturerList} />
	<Route path="/schedule" component={ScheduleList} />

	<Route path="/candidates" component={CreateCandidate} />
	<Route path="/candidates" component={DeleteCandidate} />
	<Route path="/updatecandidates" component={UpdateCandidate} />
	
	<Route path="/schedule" component={CreateSchedule} />
	<Route path="/schedule" component={DeleteSchedule} />
	<Route path="/updateschedule" component={UpdateSchedule} />

	<Route path="/updatelecturer" component={UpdateLecturer} />
	<Route path="/lecturer" component={CreateLecturer} />
	<Route path="/lecturer" component={DeleteLecturer} />

    </Router>
  );
}

export default App;
