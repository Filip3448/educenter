import React, { Component } from 'react';
import axios from 'axios';

export default class ScheduleList extends Component {
    constructor(props) {
        super(props)

        this.state = {
            schedules: [],
        }
    }

    componentDidMount() {
        axios.get('http://localhost:8080/schedule/list')
            .then(res => {
                console.log(res);
                this.setState({ schedules: res.data });
            })
            .catch((error) => {
                console.log(error);
            })
    }
    render() {
        const { schedules } = this.state;
        return (
            <div>
                <h3>Schedule</h3>
                <table className="table">
                    <thead className="thead-light">
                        <tr>
                            <th>id</th>
                            <th>Course</th>
                            <th>Location</th>
                            <th>Schedule</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            schedules.length ?
                                schedules.map(schedule => <tr>
                                    <th key={schedule.id}>{schedule.id}</th>
                                    <th key={schedule.course}>{schedule.course}</th>
                                    <th key={schedule.location}>{schedule.location}</th>
                                    <th key={schedule.schedule}>{schedule.schedule}</th>
                                </tr>) : null
                        }
                    </tbody>
                </table>
            </div>
        )
    }
}