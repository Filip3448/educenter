import React, { Component } from 'react';
import axios from 'axios';

export default class UpdateCandidate extends Component {
    constructor(props) {
        super(props);
        this.onChangeId = this.onChangeId.bind(this);
        this.onChangeFirstName = this.onChangeFirstName.bind(this);
        this.onChangeLastName = this.onChangeLastName.bind(this);
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.state = {
            id: Number,
            firstName: '',
            lastName: '',
            email: '',
        }
    }
        onChangeId(e) {
            this.setState({
                id: e.target.value
            });
        }
        onChangeFirstName(e){
            this.setState({
                firstName: e.target.value
            });
        }
        onChangeLastName(e) {
            this.setState({
                lastName: e.target.value
            });
        }
            onChangeEmail(e) {
                this.setState({
                    email: e.target.value
                });
            }
                onSubmit(e) {
                    e.preventDefault();

                    const candidates = {
                        firstName: this.state.firstName,
                        lastName: this.state.lastName,
                        email: this.state.email,
                    }
                    console.log(candidates);

            axios.put('http://localhost:8080/candidates/' + this.state.id, candidates)
                .then(res => console.log(res.data));

                    window.location = '/updatecandidates';
                }
                render() {
                    return (
                        <div>
                            <h3>Update Candidate</h3>
                            <form onSubmit={this.onSubmit}>
                                <div className="form-group">
                                    <label>Candidate Id</label>
                                    <input type="Number"
                                        required
                                        className="form-control"
                                        value={this.state.id}
                                        onChange={this.onChangeId}
                                    />
                                </div>
                                <div className="form-group">
                                    <label>First Name</label>
                                    <input type="text"
                                        required
                                        className="form-control"
                                        value={this.state.firstName}
                                        onChange={this.onChangeFirstName}
                                    />
                                </div>
                                <div className="form-group">
                                    <label>Last Name</label>
                                    <input type="text"
                                        required
                                        className="form-control"
                                        value={this.state.lastName}
                                        onChange={this.onChangeLastName}
                                    />
                                </div>
                                <div className="form-group">
                                    <label>Email</label>
                                    <input type="text"
                                        required
                                        className="form-control"
                                        value={this.state.email}
                                        onChange={this.onChangeEmail}
                                    />
                                </div>
                                <div className="form-group">
                                    <input type="submit" value="Update Candidate" className="btn btn-warning" />
                                </div>
                            </form>
                        </div>
                        )
                
    }
}