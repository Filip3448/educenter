import React, { Component } from 'react';
import axios from 'axios';

export default class DeleteCandidate extends Component {
    constructor(props) {
        super(props);
        this.onChangeId = this.onChangeId.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.state = {
            id: Number
        }
    }
    onChangeId(e) {
        this.setState({
            id: e.target.value
        });
    }
    onSubmit(e) {
        e.preventDefault();
        console.log(this.state.id);
        axios.delete('http://localhost:8080/candidates/' + this.state.id)
                .then(res => console.log(res.data));
                window.location = '/candidates';
    }
    render() {
        return (
            <div>
                <h3>Delete Candidate</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label>Candidate Id</label>
                        <input type="Number"
                            required
                            className="form-control"
                            value={this.state.id}
                            onChange={this.onChangeId}
                        />
                    </div>
                    <div className="form-group">
                                    <input type="submit" value="Delete Candidate" className="btn btn-warning" />
                                </div>
                  </form>
            </div>
        )
    }
}