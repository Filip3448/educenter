import React, {Component} from 'react';
import{Link} from 'react-router-dom';

export default class NavigationBar extends Component{
	render() {
		return(
		<nav className="navbar navbar-dark bg-primary navbar-expand-lg">
		<Link to="/" className= "navbar-barnd">Educational Center</Link>
		<div className="collapse navbar-collapse">
			<ul className ="navbar-nav mr-auto">
			<li className="navbar-item">
				<Link to= "/candidates" className="nav-link">List of Candidates</Link>
			</li>
			<li className="navbar-item">
			<Link to= "/lecturer" className="nav-link">List of Lecturers</Link>
			</li>
			<li className="navbar-item">
			<Link to= "/schedule" className="nav-link">List of Schedule</Link>
			</li>
			<li className="navbar-item">
			<Link to= "/updatecandidates" className="nav-link">Update Candidate</Link>
			</li>
			<li className="navbar-item">
			<Link to= "/updatelecturer" className="nav-link">Update Lecturer</Link>
			</li>
			<li className="navbar-item">
			<Link to= "/updateschedule" className="nav-link">Update Schedule</Link>
			</li>
			</ul>
		</div>
		</nav>
		)
	}
}