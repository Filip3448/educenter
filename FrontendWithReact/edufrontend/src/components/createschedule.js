import React, { Component } from 'react';
import axios from 'axios';

export default class CreateSchedule extends Component {
    constructor(props) {
        super(props);
         this.onChangeCourse = this.onChangeCourse.bind(this);
        this.onChangeLocation = this.onChangeLocation.bind(this);
        this.onChangeSchedule = this.onChangeSchedule.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.state = {
            
            course: '',
            location: '',
            schedule: '',
        }
    }
       
        onChangeCourse(e){
            this.setState({
                course: e.target.value
            });
        }
        onChangeLocation(e) {
            this.setState({
                location: e.target.value
            });
        }
            onChangeSchedule(e) {
                this.setState({
                    schedule: e.target.value
                });
            }
                onSubmit(e) {
                    e.preventDefault();

                    const schedule = {
                        course: this.state.course,
                        location : this.state.location,
                        schedule: this.state.schedule,
                    }
                    console.log(schedule);

            axios.post('http://localhost:8080/schedule/new', schedule)
                .then(res => console.log(res.data));

                    window.location = '/schedule';
                }
                render() {
                    return (
                        <div>
                            <h3>Create Schedule</h3>
                            <form onSubmit={this.onSubmit}>
                                <div className="form-group">
                                    <label>Course</label>
                                    <input type="text"
                                        required
                                        className="form-control"
                                        value={this.state.course}
                                        onChange={this.onChangeCourse}
                                    />
                                </div>
                                <div className="form-group">
                                    <label>Location</label>
                                    <input type="text"
                                        required
                                        className="form-control"
                                        value={this.state.location}
                                        onChange={this.onChangeLocation}
                                    />
                                </div>
                                <div className="form-group">
                                    <label>Schedule</label>
                                    <input type="text"
                                        required
                                        className="form-control"
                                        value={this.state.schedule}
                                        onChange={this.onChangeSchedule}
                                    />
                                </div>
                                <div className="form-group">
                                    <input type="submit" value="Create Schedule" className="btn btn-warning" />
                                </div>
                            </form>
                        </div>
                        )
                
    }
}