import React, { Component } from 'react';
import axios from 'axios';

export default class UpdateSchedule extends Component {
    constructor(props) {
        super(props);
        this.onChangeId = this.onChangeId.bind(this);
        this.onChangeCourse = this.onChangeCourse.bind(this);
        this.onChangeLocation = this.onChangeLocation.bind(this);
        this.onChangeSchedule = this.onChangeSchedule.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.state = {
            id: Number,
            course: '',
            location: '',
            schedule: '',
        }
    }
        onChangeId(e) {
            this.setState({
                id: e.target.value
            });
        }
        onChangeCourse(e){
            this.setState({
                course: e.target.value
            });
        }
        onChangeLocation(e) {
            this.setState({
                location: e.target.value
            });
        }
            onChangeSchedule(e) {
                this.setState({
                    schedule: e.target.value
                });
            }
                onSubmit(e) {
                    e.preventDefault();

                    const schedule = {
                        course: this.state.course,
                        location : this.state.location,
                        schedule: this.state.schedule,
                    }
                    console.log(schedule);

            axios.put('http://localhost:8080/schedule/' + this.state.id, schedule)
                .then(res => console.log(res.data));

                    window.location = '/updateschedule';
                }
                render() {
                    return (
                        <div>
                            <h3>Update Schedule</h3>
                            <form onSubmit={this.onSubmit}>
                                <div className="form-group">
                                    <label>Schedule Id</label>
                                    <input type="Number"
                                        required
                                        className="form-control"
                                        value={this.state.id}
                                        onChange={this.onChangeId}
                                    />
                                </div>
                                <div className="form-group">
                                    <label>Course</label>
                                    <input type="text"
                                        required
                                        className="form-control"
                                        value={this.state.course}
                                        onChange={this.onChangeCourse}
                                    />
                                </div>
                                <div className="form-group">
                                    <label>Location</label>
                                    <input type="text"
                                        required
                                        className="form-control"
                                        value={this.state.location}
                                        onChange={this.onChangeLocation}
                                    />
                                </div>
                                <div className="form-group">
                                    <label>Schedule</label>
                                    <input type="text"
                                        required
                                        className="form-control"
                                        value={this.state.schedule}
                                        onChange={this.onChangeSchedule}
                                    />
                                </div>
                                <div className="form-group">
                                    <input type="submit" value="Update Schedule" className="btn btn-warning" />
                                </div>
                            </form>
                        </div>
                        )
                
    }
}