import React, { Component } from 'react';
import axios from 'axios';

export default class LecturerList extends Component {
    constructor(props) {
        super(props)

        this.state = {
            lecturers: [],
        }
    }

    componentDidMount() {
        axios.get('http://localhost:8080/lecturer/list')
            .then(res => {
                console.log(res);
                this.setState({ lecturers: res.data });
            })
            .catch((error) => {
                console.log(error);
            })
    }
    render() {
        const { lecturers } = this.state;
        return (
            <div>
                <h3>Lecturers</h3>
                <table className="table">
                    <thead className="thead-light">
                        <tr>
                            <th>id</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            lecturers.length ?
                                lecturers.map(lecturer => <tr>
                                    <th key={lecturer.id}>{lecturer.id}</th>
                                    <th key={lecturer.lecturerfirstName}>{lecturer.lecturerfirstName}</th>
                                    <th key={lecturer.lecturerlastName}>{lecturer.lecturerlastName}</th>
                                    <th key={lecturer.lectureremail}>{lecturer.lectureremail}</th>
                                </tr>) : null
                        }
                    </tbody>
                </table>
            </div>
        )
    }
}