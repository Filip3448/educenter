import React, { Component } from 'react';
import axios from 'axios';

export default class CreateLecturer extends Component {
    constructor(props) {
        super(props);
        this.onChangeFirstName = this.onChangeFirstName.bind(this);
        this.onChangeLastName = this.onChangeLastName.bind(this);
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.state = {
            lecturerfirstName: '',
            lecturerlastName: '',
            lectureremail: '',
        }
    }
       
        onChangeFirstName(e){
            this.setState({
                lecturerfirstName: e.target.value
            });
        }
        onChangeLastName(e) {
            this.setState({
                lecturerlastName: e.target.value
            });
        }
            onChangeEmail(e) {
                this.setState({
                    lectureremail: e.target.value
                });
            }
                onSubmit(e) {
                    e.preventDefault();

                    const lecturer = {
                        lecturerfirstName: this.state.lecturerfirstName,
                        lecturerlastName : this.state.lecturerlastName,
                        lectureremail: this.state.lectureremail,
                    }
                    console.log(lecturer);

            axios.post('http://localhost:8080/lecturer/new' , lecturer)
                .then(res => console.log(res.data));

                    window.location = '/lecturer';
                }
                render() {
                    return (
                        <div>
                            <h3>Create Lecturer</h3>
                            <form onSubmit={this.onSubmit}>
                                <div className="form-group">
                                    <label>First Name</label>
                                    <input type="text"
                                        required
                                        className="form-control"
                                        value={this.state.lecturerfirstName}
                                        onChange={this.onChangeFirstName}
                                    />
                                </div>
                                <div className="form-group">
                                    <label>Last Name</label>
                                    <input type="text"
                                        required
                                        className="form-control"
                                        value={this.state.lecturerlastName}
                                        onChange={this.onChangeLastName}
                                    />
                                </div>
                                <div className="form-group">
                                    <label>Email</label>
                                    <input type="text"
                                        required
                                        className="form-control"
                                        value={this.state.lectureremail}
                                        onChange={this.onChangeEmail}
                                    />
                                </div>
                                <div className="form-group">
                                    <input type="submit" value="Create Lecturer" className="btn btn-warning" />
                                </div>
                            </form>
                        </div>
                        )
                
    }
}