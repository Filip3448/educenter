import React, { Component } from 'react';
import axios from 'axios';

export default class CandidateList extends Component {
    constructor(props) {
        super(props)

        this.state = {
            candidates: [],
        }
    }

    componentDidMount() {
        axios.get('http://localhost:8080/candidates/list')
            .then(res => {
                console.log(res);
                this.setState({ candidates: res.data });
            })
            .catch((error) => {
                console.log(error);
            })
    }
    render() {
        const { candidates } = this.state;
        return (
            <div>
                <h3>Candidates</h3>
                <table className="table">
                    <thead className="thead-light">
                        <tr>
                            <th>id</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            </tr>
                    </thead>
                            <tbody>
                                {
                                    candidates.length ?
                                        candidates.map(candidates => <tr>
                                            <th key={candidates.id}>{candidates.id}</th>
                                            <th key={candidates.firstName}>{candidates.firstName}</th>
                                            <th key={candidates.lastName}>{candidates.lastName}</th>
                                            <th key={candidates.email}>{candidates.email}</th>
                                        </tr>) : null
                                }
                            </tbody>
                </table>
            </div>                 
            )
    }
}