import React, { Component } from 'react';
import axios from 'axios';

export default class CreateCandidate extends Component {
    constructor(props) {
        super(props);

        this.onChangeFirstName = this.onChangeFirstName.bind(this);
        this.onChangeLastName = this.onChangeLastName.bind(this);
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.state = {
            
            firstName: '',
            lastName: '',
            email: '',
        }
    }
       
        onChangeFirstName(e){
            this.setState({
                firstName: e.target.value
            });
        }
        onChangeLastName(e) {
            this.setState({
                lastName: e.target.value
            });
        }
            onChangeEmail(e) {
                this.setState({
                    email: e.target.value
                });
            }
                onSubmit(e) {
                    e.preventDefault();

                    const candidates = {
                        firstName: this.state.firstName,
                        lastName: this.state.lastName,
                        email: this.state.email,
                    }
                    console.log(candidates);

            axios.post('http://localhost:8080/candidates/new', candidates)
                .then(res => console.log(res.data));

                    window.location = '/candidates';
                }
                render() {
                    return (
                        <div>
                            <h3>Create Candidate</h3>
                            <form onSubmit={this.onSubmit}>
                                <div className="form-group">
                                    <label>First Name</label>
                                    <input type="text"
                                        required
                                        className="form-control"
                                        value={this.state.firstName}
                                        onChange={this.onChangeFirstName}
                                    />
                                </div>
                                <div className="form-group">
                                    <label>Last Name</label>
                                    <input type="text"
                                        required
                                        className="form-control"
                                        value={this.state.lastName}
                                        onChange={this.onChangeLastName}
                                    />
                                </div>
                                <div className="form-group">
                                    <label>Email</label>
                                    <input type="text"
                                        required
                                        className="form-control"
                                        value={this.state.email}
                                        onChange={this.onChangeEmail}
                                    />
                                </div>
                                <div className="form-group">
                                    <input type="submit" value="Create Candidate" className="btn btn-warning" />
                                </div>
                            </form>
                        </div>
                        )
                
    }
}